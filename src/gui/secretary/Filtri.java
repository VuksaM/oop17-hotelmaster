package gui.secretary;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;

//CHECKSTYLE:OFF
/**
 * 
 * here, the secretary can find a specific room using filters.
 *
 */
public class Filtri {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private JLabel labelTipoCamera;
    private List<String> selezione;
    private Dimension screenSize;
    private JButton conferma;
    private JComboBox<String> combo;
    private JPanel northPanel;
    private Map<JTextField, PersonPriceDescriber> personeselezionate;
    private Image okIcon;

    private static final String NO_SELECTION = "---";

    /**
     * 
     */
    public Filtri(final SceltaCamere scelta) {
        this.frame = new JFrame("Modifica singola camera");
        this.frame.setSize(700, 600);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.personeselezionate = new HashMap<>();
        this.panel = new JPanel();
        this.panel.setBackground(Color.CYAN);
        this.northPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        this.northPanel.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.CYAN);
        List<String> extra = new ArrayList<>();
        this.selezione = new ArrayList<>();
        Set<RoomExtraPriceDescriber> set = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class);
        for (RoomExtraPriceDescriber room : set) {
            extra.add(room.getDescription());
        }
        for (String str : extra) {
            JCheckBox check = new JCheckBox(str);
            check.addActionListener(e -> {
                if (check.isSelected()) {
                    this.selezione.add(str);
                } else if (!check.isSelected()) {
                    this.selezione.remove(str);
                }
            });
            check.setBackground(Color.CYAN);
            this.panel.add(check);
        }
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/ok.png")).getImage();
        this.labelTipoCamera = new JLabel("Scegli tipo di camera da filtrare");
        final List<String> roomTypes = new ArrayList<>();
        roomTypes.add(NO_SELECTION);
        roomTypes.addAll(Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).collect(Collectors.toList()));
        this.combo = new JComboBox<>(roomTypes.toArray(new String[roomTypes.size()]));
        for (PersonPriceDescriber persona : Hotel.instance().getPriceView(PersonPriceDescriber.class)) {
            JLabel tipopersone = new JLabel(persona.getDescription());
            this.panel.add(tipopersone);
            JTextField numpersone = new JTextField(5);
            numpersone.setText("0");
            numpersone.setBackground(Color.YELLOW);
            this.panel.add(numpersone);
            personeselezionate.put(numpersone, persona);
        }
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.conferma.addActionListener(e -> {

            try {
                final Map<PersonPriceDescriber, Integer> personeinstanza = new HashMap<>();
                for (final JTextField num : personeselezionate.keySet()) {
                    final int numero = Integer.parseInt(num.getText().toString());
                    if (numero > 0) {
                        personeinstanza.put(personeselezionate.get(num), numero);
                    }
                }
                final int people = personeinstanza.values().stream().mapToInt(Integer::intValue).sum();
                if (people < 1) {
                    throw new IllegalArgumentException();
                }
                final int risp = JOptionPane.showConfirmDialog(this.frame, "Confermare ?", "Conferma",
                        JOptionPane.YES_OPTION);
                if (risp == JOptionPane.YES_OPTION) {
                    scelta.setFilters(
                            this.combo.getSelectedItem().toString().equals(NO_SELECTION) ? Optional.empty()
                                    : Optional.of(this.combo.getSelectedItem().toString()),
                            this.selezione, personeinstanza);
                    this.frame.setVisible(false);
                    this.frame.dispose();
                }
            } catch (IllegalArgumentException e1) {
                JOptionPane.showMessageDialog(this.frame, "Si deve inserire un numero positivo di persone", "Errore",
                        JOptionPane.OK_OPTION);
            }
        });
        this.southPanel.add(conferma);
        this.northPanel.add(this.labelTipoCamera);
        this.northPanel.add(this.combo);
        this.frame.getContentPane().add(this.northPanel, BorderLayout.NORTH);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.getContentPane().add(this.panel);
        this.frame.setVisible(true);
    }
}