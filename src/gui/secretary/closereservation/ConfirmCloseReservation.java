package gui.secretary.closereservation;

import gui.secretary.documentamplate.DocumentTemplate;
import hotelmaster.reservations.Stay;
import hotelmaster.reservations.StayState;

/**
 * 
 * Here, the secretary, will see the reservation of a specific document,
 * then, he/she can delete it.
 *
 */
public class ConfirmCloseReservation extends DocumentTemplate {
    /**
     * 
     * @param testo is the label text
     */
    public ConfirmCloseReservation(final String testo) {
        super(testo, StayState.INACTIVE);
    }

    @Override
    public void nextOperations(final Stay stay) {
        new SelectCloseReservation(stay);
    }

}
