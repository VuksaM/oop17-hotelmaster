package gui.admin.rooms;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import gui.admin.mainview.Scelte;
import hotelmaster.exceptions.MissingEntityException;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.RoomTemplate;

//CHECKSTYLE:OFF: MagicNumber
/**
 * 
 * here the admin can add extra component in room
 *
 */
public class AggiuntaInterni {
    private JFrame frame;
    private JPanel panel;
    private JPanel southPanel;
    private Dimension screenSize;
    private Integer numCamere;
    private Integer piano;
    private JButton conferma;
    private JButton annulla;
    private JButton esci;
    private Image okIcon;
    private Image backIcon;
    private Image exitIcon;
    private List<JCheckBox> checkBox;

    /**
     * 
     * @param camere
     *            is number of rooms that the admin want to create
     * @param piani
     *            is the number of floor/floors that the admin want to create
     */
    public AggiuntaInterni(final int camere, final int piani) {
        this.frame = new JFrame();
        this.frame.setSize(new Dimension(500, 500));
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation((screenSize.width / 2) - (this.frame.getWidth() / 2),
                (this.screenSize.height / 2) - (this.frame.getHeight() / 2));
        this.checkBox = new ArrayList<>();
        this.panel = new JPanel();
        this.panel.setBackground(Color.cyan);
        this.southPanel = new JPanel();
        this.southPanel.setBackground(Color.cyan);
        this.numCamere = camere;
        this.piano = piani;
        this.okIcon = new ImageIcon(this.getClass().getResource("/icons/add.png")).getImage();
        this.backIcon = new ImageIcon(this.getClass().getResource("/icons/back.png")).getImage();
        this.exitIcon = new ImageIcon(this.getClass().getResource("/icons/exit.png")).getImage();
        this.checkBox = Hotel.instance().getPriceView(RoomExtraPriceDescriber.class).stream().map((extra) -> {
            JCheckBox tmp = new JCheckBox(extra.getDescription());
            tmp.setBackground(Color.cyan);
            return tmp;
        }).collect(Collectors.toList());
        for (int i = 0; i < this.checkBox.size(); i++) {
            this.panel.add(this.checkBox.get(i));
        }
        this.conferma = new JButton("");
        this.conferma.setIcon(new ImageIcon(this.okIcon));
        this.annulla = new JButton("");
        this.annulla.setIcon(new ImageIcon(this.backIcon));
        this.esci = new JButton("");
        this.esci.setIcon(new ImageIcon(this.exitIcon));
        this.conferma.addActionListener(a -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Confermare?", "Conferma", JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                RoomTemplate template = RoomTemplate.create();
                for (final JCheckBox priceCheckbox : checkBox) {
                    if (priceCheckbox.isSelected()) {
                        try {
                            Optional<RoomExtraPriceDescriber> opt = Hotel.instance()
                                    .getPriceView(RoomExtraPriceDescriber.class).stream()
                                    .filter(extra -> extra.getDescription().equals(priceCheckbox.getText()))
                                    .findFirst();
                            template.addRoomExtra(opt.get());
                        } catch (MissingEntityException | IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                        System.out.println(priceCheckbox.getText());
                    }
                }
                new TipoCamera(this.piano, this.numCamere, template);
                this.frame.setVisible(false);
                this.frame.dispose();
            }
        });
        this.annulla.addActionListener(b -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere tornare indietro?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new CreaCamera();
            }
        });
        this.esci.addActionListener(c -> {
            int risp = JOptionPane.showConfirmDialog(this.frame, "Sei sicuro di volere uscire?", "Uscita",
                    JOptionPane.YES_OPTION);
            if (risp == JOptionPane.YES_OPTION) {
                this.frame.setVisible(false);
                this.frame.dispose();
                new Scelte();
            }
        });
        this.southPanel.add(this.annulla);
        this.southPanel.add(this.esci);
        this.southPanel.add(this.conferma);
        this.frame.getContentPane().add(this.panel);
        this.frame.getContentPane().add(this.southPanel, BorderLayout.SOUTH);
        this.frame.setResizable(false);
        this.frame.setVisible(true);
    }
}