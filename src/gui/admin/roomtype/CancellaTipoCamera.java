package gui.admin.roomtype;

import java.util.Optional;

import gui.admin.template.DeleteOperation;
import hotelmaster.exceptions.PriceDescriberRemovalException;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can delete a specific type room.
 *
 */
public class CancellaTipoCamera extends DeleteOperation {
    /**
     * Use a template metode to delete a type room.
     * 
     * @param testo
     *            label text
     * @param titolo
     *            frame text
     */
    public CancellaTipoCamera(final String testo, final String titolo) {
        super(testo, titolo);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendData() throws PriceDescriberRemovalException {
        Optional<RoomTypePriceDescriber> opt = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .filter(stay -> stay.getDescription().equals(this.getElementoScelto())).findFirst();
        HotelManager.create().removePriceDescriber(opt.get());
    }
}
