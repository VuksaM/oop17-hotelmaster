package gui.admin.prices;

import java.util.Optional;

import gui.admin.template.PriceOperation;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can modify the price of a specific stay extra.
 *
 */
public class PriceStayExtra extends PriceOperation {
    /**
     * 
     * @param descrizione
     *            is the text of a label that will be in PriceOperation
     */
    public PriceStayExtra(final String descrizione) {
        super(descrizione);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(StayExtraPriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendPrice(final Double price, final String name) {
        Optional<StayExtraPriceDescriber> opt = Hotel.instance().getPriceView(StayExtraPriceDescriber.class).stream()
                .filter(type -> type.getDescription().equals(name)).findFirst();
        HotelManager.create().setPriceDescriber(opt.get(), price);
    }
}