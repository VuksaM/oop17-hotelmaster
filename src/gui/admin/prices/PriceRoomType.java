package gui.admin.prices;

import java.util.Optional;

import gui.admin.template.PriceOperation;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.structure.Hotel;
import hotelmaster.structure.HotelManager;

/**
 * 
 * here the admin can modify the price of a specific room type.
 *
 */
public class PriceRoomType extends PriceOperation {
    /**
     * 
     * @param descrizione
     *            is the text of a label that will be in PriceOperation
     */
    public PriceRoomType(final String descrizione) {
        super(descrizione);
    }

    @Override
    public String[] getElements() {
        String[] array = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .map(type -> type.getDescription()).toArray(String[]::new);
        return array;
    }

    @Override
    public void sendPrice(final Double price, final String name) {
        Optional<RoomTypePriceDescriber> opt = Hotel.instance().getPriceView(RoomTypePriceDescriber.class).stream()
                .filter(type -> type.getDescription().equals(name)).findFirst();
        HotelManager.create().setPriceDescriber(opt.get(), price);
    }
}