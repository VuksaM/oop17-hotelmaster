package hotelmaster.database.utilities;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.google.common.base.Optional;

import hotelmaster.db.controller.QueryManager;
import hotelmaster.db.controller.QueryManagerImpl;
import hotelmaster.utility.time.FixedPeriod;

/**
 * Hotel statistic.
 */
public class StatisticUtilitiesImpl implements StatisticUtilities {
    private final QueryManager manager;

    /**
     * 
     */
    public StatisticUtilitiesImpl() {
        this.manager = new QueryManagerImpl();
    }

    @Override
    public double getTotalEarnings(final FixedPeriod period) {
        double sum = 0;
        ResultSet rs = null;
        final String query = "SELECT SUM (ricavo) FROM Storico WHERE date(dataFine) BETWEEN ? AND ? "
                           + "ORDER BY date(dataFine)";
        try {
            rs = manager.prepareQuery(query).date(1, period.getBeginning()).date(2, period.getEnd()).selectPrepared();
            if (rs.next()) {
                sum = rs.getDouble(1);
                rs.close();
                manager.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sum;
    }

    @Override
    public Optional<ResultSet> getOldStays() {
        final String query = "SELECT dataInizio, dataFine, ricavo, numeroPersone " + "FROM Storico "
                           + "ORDER BY date(dataInizio)";
        ResultSet rs = manager.createQuery().selectNotPrepared(query);
        if (rs != null) {
            return Optional.of(rs);
        } else {
            return Optional.absent();
        }
    }

}
