package hotelmaster.structure;

import java.time.LocalTime;
import java.util.Map;
import java.util.Set;

import hotelmaster.database.factory.DataFactory;
import hotelmaster.pricing.PriceDescriber;
import hotelmaster.reservations.Client;
import hotelmaster.reservations.DocumentType;
import hotelmaster.reservations.Stay;

/**
 * The hotel containing all of its {@link PriceDescriber}s, {@link Room}s,
 * {@link Stay}s and {@link Client}s. The first thing any application relying on
 * a {@link Hotel} should do is set a valid {@link DataFactory} through
 * {@link Hotel#setDataSource(DataFactory)}. Failing to comply will result in
 * some kind of exception or other unpleasant outcome.
 */
public interface Hotel {

    /**
     * Returns the floors in the hotel and the highest room number in each
     * floor.
     * 
     * @return the floors in an immutable map
     */
    Map<Integer, Integer> getFloorView();

    /**
     * Returns the types of documents avaliable.
     * 
     * @return the documents in an immutable set
     */
    Set<DocumentType> getDocuments();

    /**
     * Returns the PriceDescribers of a certain given type. This Set may not be
     * modified.
     * 
     * @param <T>
     *            the type of the price describer
     * @param priceType
     *            the type of the price describers to be returned
     * @return the PriceDescribers in an unmodifiable set
     */
    <T extends PriceDescriber> Set<T> getPriceView(Class<T> priceType);

    /**
     * Returns whether this hotel has a given PriceDescriber.
     * 
     * @param <T>
     *            the type of the price describer
     * @param price
     *            the PriceDescriber
     * @return the existence of the PriceDescriber
     */
    <T extends PriceDescriber> boolean hasPriceDescriber(T price);

    /**
     * Returns all the rooms in the hotel. This Set may not be modified.
     * 
     * @return the unmodifiable set of rooms
     */
    Set<Room> getRoomView();

    /**
     * Returns all the stays in the hotel. This Set may not be modified.
     * 
     * @return the unmodifiable set of stays
     */
    Set<Stay> getStayView();

    /**
     * Returns all the clients which are or have been in the hotel. This Set may
     * not be modified.
     * 
     * @return the unmodifiable set of clients
     */
    Set<Client> getClientView();

    /**
     * Sets the hotel's data source to a given {@link DataFactory}.
     * 
     * @param data
     *            the data factory
     * @throws IllegalStateException
     *             the hotel is already in use
     */
    void setDataSource(DataFactory data) throws IllegalStateException;

    /**
     * Sets the stay cleanup function to the given time.
     * 
     * @param callTime
     *            the time when the cleanup will be called
     */
    void setStayCleanup(LocalTime callTime);

    /**
     * Returns the singleton instance of the hotel, accordingly loaded.
     * 
     * @return the new Hotel instance
     */
    static Hotel instance() {
        return ModifiableHotel.instance();
    }
}
