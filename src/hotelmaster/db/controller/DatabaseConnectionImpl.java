package hotelmaster.db.controller;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.sqlite.SQLiteConfig;

/**
 * Manages the connectivity with the database.
 */
public final class DatabaseConnectionImpl implements DatabaseConnection {

    private static DatabaseConnectionImpl db;
    private Connection conn = null;
    private static final String URL = "jdbc:sqlite:";
    private static final String DB_NAME = "hotelmaster.db";
    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String DB_PATH = System.getProperty("user.home") + File.separator + DB_NAME;
    private static final File FILE = new File(DB_PATH);

    /**
     * 
     * @return true if database has already been created
     */
    public static boolean isDatabaseLoaded() {
        return FILE.exists();
    }

    /**
     * Solution to enable foreign keys for the dbms that we are going to use:
     * code taken by
     * http://code-know-how.blogspot.it/2011/10/how-to-enable-foreign-keys-in-sqlite3.html
     */
    private DatabaseConnectionImpl() {
        try {
            Class.forName(DRIVER).newInstance();
            SQLiteConfig config = new SQLiteConfig();
            config.enforceForeignKeys(true);
            this.conn = DriverManager.getConnection(URL + DB_PATH, config.toProperties());
            System.out.println("Connection to the database " + DB_NAME + " established");

        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    /**
     * @return the Singleton of this class.
     */
    public static DatabaseConnectionImpl getSingleton() {
        if (db == null) {
            db = new DatabaseConnectionImpl();
        }
        return db;
    }

    @Override
    public Connection getConnection() {
        return this.conn;
    }

    @Override
    public void closeConnection() {
        try {
            if (conn != null && !this.conn.isClosed()) {
                try {
                    this.conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
