package hotelmaster.db.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import hotelmaster.database.admin.AccountManagerImpl;
import hotelmaster.database.factory.DatabaseFactory;
import hotelmaster.exceptions.AccountException;
import hotelmaster.exceptions.GuestException;
import hotelmaster.exceptions.RoomRemovalException;
import hotelmaster.pricing.PersonPriceDescriber;
import hotelmaster.pricing.RoomExtraPriceDescriber;
import hotelmaster.pricing.RoomTypePriceDescriber;
import hotelmaster.pricing.SeasonPriceDescriber;
import hotelmaster.pricing.StayExtraPriceDescriber;
import hotelmaster.pricing.StayTypePriceDescriber;
import hotelmaster.reservations.Client;
import hotelmaster.reservations.DocumentType;
import hotelmaster.reservations.ModifiableOccupation;
import hotelmaster.reservations.ModifiableStay;
import hotelmaster.structure.ModifiableRoom;
import hotelmaster.utility.time.FixedPeriod;

//CHECKSTYLE:OFF: MagicNumber
/**
 * Demo version.
 */
public class DemoVersion {

    private final QueryManager manager;

    private final DocumentType passaporto;
    private final DocumentType patente;
    private final DocumentType carta;

    private final RoomExtraPriceDescriber vistaMare;
    private final RoomExtraPriceDescriber balcone;

    private final StayExtraPriceDescriber parcheggio;
    private final StayExtraPriceDescriber colazione;
    private final StayExtraPriceDescriber piscina;
    private final StayExtraPriceDescriber animali;

    private final RoomTypePriceDescriber singola;
    private final RoomTypePriceDescriber doppia;
    private final RoomTypePriceDescriber matrimoniale;
    private final RoomTypePriceDescriber familiare;

    private final StayTypePriceDescriber completa;
    private final StayTypePriceDescriber mezza;
    private final StayTypePriceDescriber pernottamento;
    private final StayTypePriceDescriber allInclusive;

    private final PersonPriceDescriber adulto;
    private final PersonPriceDescriber bambino;
    private final PersonPriceDescriber ragazzo;

    private final ModifiableRoom room11;
    private final ModifiableRoom room12;
    private final ModifiableRoom room13;
    private final ModifiableRoom room14;
    private final ModifiableRoom room21;
    private final ModifiableRoom room22;
    private final ModifiableRoom room23;
    private final ModifiableRoom room24;
    private final ModifiableRoom room25;

    private final ModifiableStay stay1;
    private final ModifiableStay stay2;
    private final ModifiableStay stay3;
    private final ModifiableStay stay4;
    private final ModifiableStay stay5;

    private final Client c1;
    private final Client c2;
    private final Client c3;
    private final Client c4;
    private final Client c5;

    /**
     * 
     */
    public DemoVersion() {
        this.manager = new QueryManagerImpl();

        passaporto = new DocumentType("Passaporto", 9);
        carta = new DocumentType("Carta D'Identità", 7);
        patente = new DocumentType("Patente", 10);

        vistaMare = new RoomExtraPriceDescriber("Vista mare", 10);
        balcone = new RoomExtraPriceDescriber("Balcone", 5);
        parcheggio = new StayExtraPriceDescriber("Parcheggio", 7.50, false);
        colazione = new StayExtraPriceDescriber("Colazione a buffet", 5.0, true);
        piscina = new StayExtraPriceDescriber("Piscina", 6, true);
        animali = new StayExtraPriceDescriber("Animali in camera", 4.50, false);

        singola = new RoomTypePriceDescriber("Singola", 50, 0.4, 1);
        doppia = new RoomTypePriceDescriber("Doppia", 40, 0.3, 2);
        matrimoniale = new RoomTypePriceDescriber("Matrimoniale", 35, 0.2, 2);
        familiare = new RoomTypePriceDescriber("Familiare", 30, 0.15, 3);

        completa = new StayTypePriceDescriber("Pensione completa", 1);
        mezza = new StayTypePriceDescriber("Mezza pensione", 0.8);
        pernottamento = new StayTypePriceDescriber("Pernottamento", 0.5);
        allInclusive = new StayTypePriceDescriber("All inclusive", 1.2);

        adulto = new PersonPriceDescriber("Adulto", 1);
        bambino = new PersonPriceDescriber("Bambino 5-10", 0.3);
        ragazzo = new PersonPriceDescriber("Ragazzo 10-14", 0.7);

        room11 = ModifiableRoom.create(0, 1);
        room12 = ModifiableRoom.create(0, 2);
        room13 = ModifiableRoom.create(0, 3);
        room14 = ModifiableRoom.create(0, 4);
        room21 = ModifiableRoom.create(1, 1);
        room22 = ModifiableRoom.create(1, 2);
        room23 = ModifiableRoom.create(1, 3);
        room24 = ModifiableRoom.create(1, 4);
        room25 = ModifiableRoom.create(1, 5);

        room11.setType(doppia);
        room11.getExtras().add(this.balcone);
        room11.getExtras().add(this.vistaMare);
        room12.setType(familiare);
        room13.setType(familiare);
        room13.getExtras().add(vistaMare);
        room14.setType(singola);
        room21.setType(matrimoniale);
        room22.setType(familiare);
        room22.getExtras().add(vistaMare);
        room23.setType(matrimoniale);
        room24.setType(doppia);
        room24.getExtras().add(balcone);
        room25.setType(doppia);

        c1 = Client.create("Lorenzo D'Agnolo", "Italia", patente, "DG658GBU76", "337353202");
        c2 = Client.create("Emanuel Ivanaj", "Italia", passaporto, "IV46739FG", "3321345879");
        c3 = Client.create("Vuksa Mihajlovic", "Italia", carta, "ML6589H", "332567898");
        c4 = Client.create("Fujiwara Takeshi", "Giappone", passaporto, "FU847635T", "337353202");
        c5 = Client.create("Alex Moore", "Inghilterra", patente, "MO746523TG", "3124563452");

        stay1 = ModifiableStay.create();
        stay2 = ModifiableStay.create();
        stay3 = ModifiableStay.create();
        stay4 = ModifiableStay.create();
        stay5 = ModifiableStay.create();

        stay1.setClient(c1);
        stay2.setClient(c2);
        stay3.setClient(c3);
        stay4.setClient(c4);
        stay5.setClient(c5);

        stay1.setDates(FixedPeriod.of(LocalDate.of(2017, 06, 10), LocalDate.of(2017, 06, 17)));
        stay2.setDates(FixedPeriod.of(LocalDate.of(2017, 06, 15), LocalDate.of(2017, 06, 30)));
        stay3.setDates(FixedPeriod.of(LocalDate.of(2017, 07, 17), LocalDate.of(2017, 07, 22)));
        stay4.setDates(FixedPeriod.of(LocalDate.of(2017, 07, 01), LocalDate.of(2017, 07, 8)));
        stay5.setDates(FixedPeriod.of(LocalDate.of(2017, 06, 18), LocalDate.of(2017, 06, 28)));

        stay1.setType(completa);
        stay2.setType(allInclusive);
        stay3.setType(mezza);
        stay4.setType(completa);
        stay5.setType(pernottamento);

        stay1.getExtras().add(parcheggio);
        stay1.getExtras().add(piscina);
        stay3.getExtras().add(animali);
        stay4.getExtras().add(colazione);
        stay4.getExtras().add(parcheggio);
        stay5.getExtras().add(animali);
        Map<PersonPriceDescriber, Integer> map = new HashMap<>();
        map.put(adulto, 2);
        stay1.getOccupations().add(ModifiableOccupation.create(stay1, room11, map));
        map.put(ragazzo, 1);
        stay2.getOccupations().add(ModifiableOccupation.create(stay2, room22, map));
        map.put(bambino, 1);
        stay3.getOccupations().add(ModifiableOccupation.create(stay3, room12, map));
        map.clear();
        map.put(adulto, 1);
        stay4.getOccupations().add(ModifiableOccupation.create(stay4, room14, map));
        map.put(ragazzo, 1);
        stay5.getOccupations().add(ModifiableOccupation.create(stay4, room11, map));
    }
    
    /**
     * Complete the database creation (demo version).
     */
    public void complete() {
        this.createAdminAccount();
        this.createUserAccount();
        this.createDocuments();
        this.createRoomExtraPriceDescribers();
        this.createSesonPriceDescribers();
        this.createStayExtraPriceDescribers();
        this.createRoomTypePriceDescribers();
        this.createStayTypePriceDescribers();
        this.createPersonPriceDescribers();
        this.createRooms();
        this.createStays();
        this.oldStaysLoader(LocalDate.of(2016, 06, 30),LocalDate.of(2016, 07, 10),1000,4);
        this.oldStaysLoader(LocalDate.of(2016, 07, 18),LocalDate.of(2016, 07, 23),800,3);
        this.oldStaysLoader(LocalDate.of(2016, 9, 18),LocalDate.of(2016, 9, 25),500,4);
        this.oldStaysLoader(LocalDate.of(2016, 8, 9),LocalDate.of(2016, 8,15),1500,5);
    }
    
    /**
     * Basic database creation (empty version).
     */
    public void basicComplete() {
        this.createAdminAccount();
        this.createDocuments();
    }
    
    private void oldStaysLoader(LocalDate dataInizio, LocalDate dataFine, double ricavo, int numeroPersone) {
         String query = "INSERT INTO Storico (dataInizio, dataFine, ricavo, numeroPersone) VALUES (?,?,?,?)";
         try {
            manager.prepareQuery(query).date(1, dataInizio)
                                        .date(2, dataFine )
                                        .price(3, ricavo)
                                        .integer(4, numeroPersone)
                                        .update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createDocuments() {
        final String query = "INSERT INTO TipoDocumento (descrizione,numeroCaratteri) VALUES (?, ?)";
        try {
            manager.prepareQuery(query).string(1, "Passaporto").integer(2, 9).update();
            manager.prepareQuery(query).string(1, "Carta D'Identità").integer(2, 7).update();
            manager.prepareQuery(query).string(1, "Patente").integer(2, 10).update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createAdminAccount() {
        final String query = "INSERT INTO Account (tipo, username, password) " + "VALUES (?,?,?)";
        try {
            manager.prepareQuery(query).integer(1, 0).string(2, "admin").string(3, "admin").update();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void createUserAccount() {
        try {
            new AccountManagerImpl().addUserAccount("secretary", "secretary");
        } catch (AccountException e) {
            e.printStackTrace();
        }
    }

    private void createRoomExtraPriceDescribers() {
        new DatabaseFactory().getPrices().create(vistaMare);
        new DatabaseFactory().getPrices().create(balcone);
    }

    private void createStayExtraPriceDescribers() {
        new DatabaseFactory().getPrices().create(parcheggio);
        new DatabaseFactory().getPrices().create(colazione);
        new DatabaseFactory().getPrices().create(animali);
        new DatabaseFactory().getPrices().create(piscina);
    }

    private void createRoomTypePriceDescribers() {
        new DatabaseFactory().getPrices().create(singola);
        new DatabaseFactory().getPrices().create(doppia);
        new DatabaseFactory().getPrices().create(matrimoniale);
        new DatabaseFactory().getPrices().create(familiare);
    }

    private void createStayTypePriceDescribers() {
        new DatabaseFactory().getPrices().create(completa);
        new DatabaseFactory().getPrices().create(mezza);
        new DatabaseFactory().getPrices().create(pernottamento);
        new DatabaseFactory().getPrices().create(allInclusive);
    }

    private void createPersonPriceDescribers() {
        new DatabaseFactory().getPrices().create(adulto);
        new DatabaseFactory().getPrices().create(bambino);
        new DatabaseFactory().getPrices().create(ragazzo);
    }

    private void createSesonPriceDescribers() {
        new DatabaseFactory().getPrices().create(new SeasonPriceDescriber("Estate 2017", 1.5,
                FixedPeriod.of(LocalDate.of(2017, 05, 01), LocalDate.of(2017, 9, 15))));
        new DatabaseFactory().getPrices().create(new SeasonPriceDescriber("Autunno 2017", 1,
                FixedPeriod.of(LocalDate.of(2017, 9, 16), LocalDate.of(2017, 12, 15))));
        new DatabaseFactory().getPrices().create(new SeasonPriceDescriber("Inverno 2018", 0.75,
                FixedPeriod.of(LocalDate.of(2017, 12, 16), LocalDate.of(2018, 03, 21))));
        new DatabaseFactory().getPrices().create(new SeasonPriceDescriber("Primavera 2018", 1,
                FixedPeriod.of(LocalDate.of(2018, 03, 22), LocalDate.of(2018, 05, 30))));
    }

    private void createRooms() {

        try {
            new DatabaseFactory().getRooms().createRoom(room11);
            new DatabaseFactory().getRooms().createRoom(room12);
            new DatabaseFactory().getRooms().createRoom(room13);
            new DatabaseFactory().getRooms().createRoom(room14);
            new DatabaseFactory().getRooms().createRoom(room21);
            new DatabaseFactory().getRooms().createRoom(room22);
            new DatabaseFactory().getRooms().createRoom(room23);
            new DatabaseFactory().getRooms().createRoom(room24);
            new DatabaseFactory().getRooms().createRoom(room25);
        } catch (RoomRemovalException | IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    private void createStays() {
        try {
            new DatabaseFactory().getReservation().registerStay(stay1);
            new DatabaseFactory().getReservation().registerStay(stay2);
            new DatabaseFactory().getReservation().registerStay(stay3);
            new DatabaseFactory().getReservation().registerStay(stay4);
            new DatabaseFactory().getReservation().registerStay(stay5);
        } catch (GuestException e) {
            e.printStackTrace();
        }
    }
    // CHECKSTYLE:ON: MagicNumber
}
